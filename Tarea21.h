struct Auto
{
  char Marca[60];
  char  Modelo[60];
  char  Anno[60];
  char  Motor[60];
};
struct Computadora
{
  char Modelo[60];
  char tipo[60];
  char Complementos[60];
  char Procesador[60];
};
struct Alumno
{
  char Nombre[50];
  int edad[2];
  char genero[30];
  char carrera[30];
  int numero_de_cuenta[10];
};
struct Equipo_de_futbol
{
  char Nombre[50];
  int Alineacion[10];
  char liga[40];
  int copas[30];
};
struct Mascota
{
  char Nombre[30];
  char Animal[40];
  char raza[40];
  char color[40];
};
struct Video_juego
{
  char Nombre[40];
  int Jugadores[2];
  char Modos_de_juego[30];
  int anno_de_lanzamiento[4];
};
struct Super_Heroe
{
  char Nombre[40];
  char Poderes[40];
  char Equipo[30];
  char Aydantes[30];
};
struct Planetas
{
  char Nombre[30];
  char Constelacion[30];
  char galaxia[30];
  int Distancia_tierra[20];
  int Diametro[30];
};
struct Personaje_de_su_juego_favorito
{
  char Nombre_personaje[40];
  char Nombre_del_juego[40];
  char Poderes[40];
  char Debilidades[40];
};
struct comic
{
  char Nombre[40];
  char Personaje_principal[40];
  int anno_de_lanzamiento[40];
  int Edicion[40];
};
